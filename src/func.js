const getSum = (str1, str2) => {
  if (typeof str1 != "string" || typeof str2 != "string") {
    return false;
  }
  if (str1.length == 0 || str2.length == 0) {
    return str1+str2;
  }

  let bufer = 0;
  let result = "";
  let reversStr1 = Array.from(str1).reverse();
  let reversStr2 = Array.from(str2).reverse();
  let currentNum = 0;

  for (let i = 0; i < str1.length; i++) {
    if (isNaN(+reversStr1[i]) || isNaN(+reversStr2[i])) {
      return false;
    }
    currentNum = +reversStr1[i] + +reversStr2[i] + bufer;
    if (currentNum <= 9) {
      result += currentNum;
      bufer = 0;
    }
    else {
      result += currentNum % 10;
      bufer = 1;
    }
  }
  if (bufer != 0) {
    result += bufer;
  }
  return Array.from(result).reverse().reduce((prev, curr) => prev+curr,"");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCounter = 0;
  let commCounter = 0;
  for (let i in listOfPosts) {
    if (listOfPosts[i].author === authorName) {
      postCounter++;
    }
    if (listOfPosts[i].comments != undefined) {
      for (let comm of listOfPosts[i].comments) {
        if (comm.author == authorName) {
          commCounter++;
        }
      }
    }
  }
  return "Post:" + postCounter+",comments:" + commCounter;
};

const tickets=(people)=> {
  let changeBank = 0;
  const ticketPrice = 25;
  for (let customer of people) {
    if (customer - changeBank > ticketPrice) {
      return "NO";
    }
    else {
      if (customer == ticketPrice) {
        changeBank += customer;
      }
      changeBank = changeBank + customer - ticketPrice;
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
